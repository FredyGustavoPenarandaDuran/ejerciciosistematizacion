/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistematizacionparlamentaria;

/**
 *
 * @author estudiante
 */
public class SistematizacionParlamentaria {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Partido p = new Partido("Conservador Liberal de la Alianza Democrática ", "2010");
        Partido p2 = new Partido("Centro Esperanza", "2009");
        Departamento d = new Departamento("Norte de santander", 5);
        Senado s1 = new Senado("Jorge Eliecer Pérez", "33445566 ", "jorge@gmail.com ", "312441551", "M", p, 99);
        Senado s2 = new Senado("Diego Marquez", "778899 ", "diego@gmail.com ", "312441551", "M", p2, 73);
        Camara c1 = new Camara("Fredy Peñaranda Duran", "10052818", "fredy@gmail.com ", "305239181", "M", p, 101,d);
        Camara c2 = new Camara("Jhon Alexis", "112233", "jhon@gmail.com ", "31841521", "M", p2, 106,d);
        
        //Primer Senador 
        System.out.println("SENADOR 1");
        s1.validarNumero();
        //Registro 4 votos
        s1.registrarVoto();
        s1.registrarVoto();
        s1.registrarVoto();
        s1.registrarVoto();
        s1.imprimirDatos();
        
        //Segundo Senador 
        System.out.println("");
        System.out.println("SENADOR 2");
        s2.validarNumero();
        //Registro 3 votos
        s2.registrarVoto();
        s2.registrarVoto();
        s2.registrarVoto();
        s2.imprimirDatos();
        
       
        //Representante a camara 1
        System.out.println("");
        System.out.println("REPRESENTANTE A CÁMARA 1:");
        c1.validarNumero();
        //Registro 5 votos
        c1.registrarVoto();
        c1.registrarVoto();
        c1.registrarVoto();
        c1.registrarVoto();
        c1.registrarVoto();
        c1.imprimirDatos();
        
        //Representante a camara 1
        System.out.println("");
        System.out.println("REPRESENTANTE A CÁMARA 2:");
        c2.validarNumero();
        //Registro 7 votos
        c2.registrarVoto();
        c2.registrarVoto();
        c2.registrarVoto();
        c2.registrarVoto();
        c2.registrarVoto();
        c2.registrarVoto();
        c2.registrarVoto();
        c2.imprimirDatos();
        
        //Porcentaje de votos
        System.out.println("");
        System.out.println("El porcentaje de votos del 1er senador fue de: "+s1.calcularPorcentajeVotos()+" %");
        System.out.println("El porcentaje de votos del 2do senador fue de: "+s2.calcularPorcentajeVotos()+" %");
        System.out.println("");
        System.out.println("El porcentaje de votos del 1er representante a camara fue de: "+c1.calcularPorcentajeVotos()+" %");
        System.out.println("El porcentaje de votos del 2do representante a camara fue de: "+c2.calcularPorcentajeVotos()+" %");
    }
    
}
